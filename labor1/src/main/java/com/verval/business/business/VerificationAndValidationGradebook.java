package com.verval.business.business;

import com.verval.business.exception.NotEligibleStudentException;

import java.util.ArrayList;
import java.util.OptionalDouble;

public class VerificationAndValidationGradebook {

    private ArrayList<VerificationAndValidationGradeCalculator> vAndVGradeCalculators;
    private int minimumYearOfStudyForCourse;

    public VerificationAndValidationGradebook(int minimumYearOfStudyForCourse) {
        vAndVGradeCalculators = new ArrayList<>();
        this.minimumYearOfStudyForCourse = minimumYearOfStudyForCourse;
    }

    public ArrayList<VerificationAndValidationGradeCalculator> getvAndVGradeCalculators() {
        return vAndVGradeCalculators;
    }

    public int getMinimumYearOfStudyForCourse() {
        return minimumYearOfStudyForCourse;
    }

    public void setMinimumYearOfStudyForCourse(int minimumYearOfStudyForCourse) {
        this.minimumYearOfStudyForCourse = minimumYearOfStudyForCourse;
    }

    public void addvAndVGradeCalculator(VerificationAndValidationGradeCalculator vAndVGradeCalculator, boolean exceptionStudent) {
        if(vAndVGradeCalculator.getYearOfStudy(exceptionStudent) >= minimumYearOfStudyForCourse) {
            vAndVGradeCalculators.add(vAndVGradeCalculator);
        }
        else {
            throw new  NotEligibleStudentException("Student does not meet the minimum year of study needed to take the course");
        }
    }

    public OptionalDouble getAverageOfGrades() {
        return vAndVGradeCalculators.stream().mapToDouble(VerificationAndValidationGradeCalculator::getGrade).average();
    }

    public long getCountOfPassingGrades() {
        return vAndVGradeCalculators.stream().filter(VerificationAndValidationGradeCalculator::isPassing).count();
    }

}
