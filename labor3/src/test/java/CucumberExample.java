import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pom.ExampleSignInPage;

public class CucumberExample {

    private WebDriver webdriver;
    private ExampleSignInPage exampleSignInPage;

    @Given("^The page opened$")
    public void openPage() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webdriver = new ChromeDriver();
        webdriver.navigate().to("url");
        exampleSignInPage = new ExampleSignInPage(webdriver);
    }

    @When("I enter username {string}")
    public void enterUsername(String username) {
        System.out.println("I enter username ");
        exampleSignInPage.writeTextToUsernameField(username);
    }

    @And("^I enter password \"(.*)\"")
    public void enterPassword(String password) {
        exampleSignInPage.writeTextToPasswordField(password);
        System.out.println("I enter password ");
    }

    @Then("I log in")
    public void logIn() {
        exampleSignInPage.pressSignInButton();
        System.out.println("I log in");
    }

}
